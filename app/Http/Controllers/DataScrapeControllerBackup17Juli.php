<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 0); // Unlimited execution time

use App\Product;
use DB;
use File;
use Input;
use Intervention\Image\Exception\NotReadableException;
use Response;
use Storage;

class DataScrapeController extends Controller {
	const IMG_PATH = 'public/uploads/';
	const CATEGORY_NAME = 'Jam Tangan';
	protected $colormap;
	public function __construct() {
		$this->load_colormap();
	}

	public function get_tokopedia_merchant_test() {

		/*$image = ColorExtractor::loadPng('http://www.pngall.com/wp-content/uploads/2016/04/Women-Bag-High-Quality-PNG-180x180.png');
			$palette = $image->extract();
			print_r($palette);

			$hex = "#CFC34E";

			list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
			echo "$hex -> $r $g $b";
			echo "<hr/>";

			echo "haso :" . self::hex_to_name("#f0f8ff");

			echo '<span style="color:#E7866D;text-align:center;">Request has been sent. Please wait for my reply!</span>';

		*/

		DB::table('product')->update(['PRODUCT_STATUS_TEMP' => 0]);
		DB::table('master_category')->update(['CATEGORY_STATUS_TEMP' => 0]);

		$merchant_url = 'https://ace.tokopedia.com/search/v2.6/product?shop_id=202414&ob=10&rows=80&start=0&full_domain=www.tokopedia.com&scheme=https&device=desktop&source=shop_product';

		$curl_data = $this->get_data($merchant_url);

		if (!$curl_data) // nothing grabbing
		{
			// display error message
			return Response::json(array(
				'code' => 503,
				'date' => date('Y-m-d h:i:sa'),
				'message' => 'Connection Error',

			));
			// send telegram report
		}

		$result = json_decode($curl_data);

		$number = 0;

		foreach ($result->data as $key => $val) {

			//
			$product_check_available = DB::table('product')
				->where('PRODUCT_MERCHANT_ID', '=', $val->id)
				->exists();

			DB::table('product')
				->where('PRODUCT_MERCHANT_ID', '=', $val->id)
				->update(['PRODUCT_STATUS_TEMP' => 1]);

			if ($product_check_available == TRUE) {
				$check_main_image = "cjt-" . $val->id;
				$check_main_image_hash = "img_" . $this->cryptor('encrypt', $check_main_image) . "_220x290.jpg";

				$image_exists = Storage::disk('local')->exists(self::IMG_PATH . '' . $check_main_image_hash);

				if ($image_exists) {

					// picture exists
					echo "gambar ada";

					continue;

				} else {

					/*$main_image="cjt-".$val->id;
								$main_image_hash ="img_".$this->cryptor('encrypt',$main_image);
						        $main_image_hash_300X400 = "img_".$this->cryptor('encrypt',$main_image)."_300x400.jpg";
					*/
					$folder_220X290 = 'storage/uploads/' . $check_main_image_hash;
					//$folder_600X500 = 'storage/uploads/'.$main_image_hash_600X500;
					$toped_image = $val->image_uri_700; // maybe stored in parameter
					try
					{
						$image_220x290 = \Image::make($toped_image);
						//$image_600x500 = \Image::make($toped_image);
					} catch (NotReadableException $e) {
						// If error, stop and continue looping to next iteration
						continue;
					}
					// If no error ...
					$image_220x290->fit(220, 290, function ($constraint) {
						$constraint->aspectRatio();
					});

					/*  $image_600x500->fit(600, 500, function ($constraint)
						        {
						        	$constraint->aspectRatio();
					*/
					$image_220x290->save($folder_220X290);
					//$image_600x500->save($folder_600X500);
					echo "ditambahkan";

				}
				continue;

			} else {
				// --------------------< G E T  C A T E G O R Y >--------------------------
				$url_category = 'https://www.tokopedia.com/ajax/product-prev-next.pl?p_id=' . $val->id . '&action=prev_next&s_id=202414&lang=id';

				/*$url_category = 'https://www.tokopedia.com/ajax/product-prev-next.pl?p_id=306328496&action=prev_next&s_id=202414&lang=id';*/

				echo "masuk sini pengecekan di category";

				$get_data_category = $this->get_data($url_category);

				if (!preg_match('/titl.*?"(.*?)"/', $get_data_category)) {
					continue;

				}
				preg_match('/titl.*?"(.*?)"/', $get_data_category, $result_data_category);

				$get_merch = rtrim($result_data_category[1], '\\');

				if (substr($get_merch, 0, 10) === 'JAM TANGAN') {
					$get_merch = preg_replace("/\s\(.*?\)/", "", $get_merch);

					$get_merch = str_replace("JAM TANGAN ", "", $get_merch);
				} else {
					echo " tidak sesuai category";

					continue;

				}

				// --------------------< / G E T  C A T E G O R Y >-------------------------
				goto SECTION_IMAGE;
			}

			//dd($product_check_available);

			//echo count($product_check_available);
			//die;
			// --------------------< I M A G E >---------------------------------------

			SECTION_IMAGE:

			$main_image = "cjt-" . $val->id;
			$main_image_hash = "img_" . $this->cryptor('encrypt', $main_image);
			$main_image_hash_300X400 = "img_" . $this->cryptor('encrypt', $main_image) . "_300x400.jpg";
			$main_image_hash_220X290 = "img_" . $this->cryptor('encrypt', $main_image) . "_220X290.jpg";
			$folder_300X400 = 'storage/uploads/' . $main_image_hash_300X400;
			$folder_220X290 = 'storage/uploads/' . $main_image_hash_220X290;
			$toped_image = $val->image_uri_700; // maybe stored in parameter

			try
			{
				$image_300x400 = \Image::make($toped_image);
				$image_220X290 = \Image::make($toped_image);
			} catch (NotReadableException $e) {
				// If error, stop and continue looping to next iteration
				continue;
			}
			// If no error ...
			$image_300x400->fit(300, 400, function ($constraint) {
				$constraint->aspectRatio();
			});

			$image_220X290->fit(220, 290, function ($constraint) {
				$constraint->aspectRatio();
			});
			$image_300x400->save($folder_300X400);
			$image_220X290->save($folder_220X290);

			// --------------------< / I M A G E>--------------------------------------

			// --------------------<  D E T A I L  P R O D U C T >----------------------

			$url_detail = $this->get_data($val->uri);

			$format_regex = $this->clean_data($url_detail);

			preg_match('/\<div itemscope (.*) diskusi/', $format_regex, $result_description);

			preg_match('/itemprop="description">(.*?)rev/', $result_description[1], $result_description_match);

			$result_description_match[1];

			// --------------------< / D E T A I L  P R O D U C T >---------------------

			// --------------------< P R I C E  M A R K U P >---------------------------

			$val->price = preg_replace('/Rp/', '', $val->price);

			$val->price = preg_replace('/\./', '', $val->price);

			$toped_supplier_price = $val->price;

			if ($toped_supplier_price <= 50000) {

				$markup_percentige = 45;
			} else if (($toped_supplier_price > 50000) AND ($toped_supplier_price <= 125000)) {

				$markup_percentige = 35;
			} else if (($toped_supplier_price > 125000) AND ($toped_supplier_price <= 250000)) {

				$markup_percentige = 30;
			} else {

				$markup_percentige = 25;
			}

			// ------------------< / P R I C E  M A R K U P >--------------------------

			// --------------------< G E T  P R O D U C T  V A L U E >-----------------

			$category_check_available = DB::table('master_category')
				->where('CATEGORY_MERCH', '=', $get_merch)

				->exists();

			if ($category_check_available === TRUE) {

				DB::table('master_category')
					->where('CATEGORY_MERCH', '=', $get_merch)
					->update(['CATEGORY_STATUS_TEMP' => 1]);
				$get_id_category = DB::table('master_category')
					->select('CATEGORY_ID')
					->where('CATEGORY_MERCH', '=', $get_merch)
					->first();
				$FIELD_PRODUCT_ID_CATEGORY = $get_id_category->CATEGORY_ID;
			} else {
				$FIELD_CATEGORY_MERCH = $get_merch;
				$FIELD_CATEGORY_MERCH_SLUG = str_slug($FIELD_CATEGORY_MERCH, "-");
				$FIELD_CATEGORY_NAME = self::CATEGORY_NAME;
				$FIELD_CATEGORY_NAME_SLUG = str_slug($FIELD_CATEGORY_NAME, "-");
				$FIELD_PRODUCT_ID_CATEGORY = DB::table('master_category')->insertGetId(
					['CATEGORY_NAME' => $FIELD_CATEGORY_NAME, 'CATEGORY_NAME_SLUG' => $FIELD_CATEGORY_NAME_SLUG, 'CATEGORY_MERCH' => $FIELD_CATEGORY_MERCH, 'CATEGORY_MERCH_SLUG' => $FIELD_CATEGORY_MERCH_SLUG, 'CATEGORY_STATUS_TEMP' => 1]
				);
			}

			$FIELD_PRODUCT_NAME = $val->name;
			$FIELD_PRODUCT_NAME_SLUG = str_slug($FIELD_PRODUCT_NAME, "-");
			$FIELD_PRODUCT_MERCHANT_PRICE = $val->price;
			$FIELD_PRODUCT_CJT_PRICE = $val->price * ((100 + $markup_percentige) / 100);
			$FIELD_PRODUCT_MERCHANT_ID = $val->id;
			$FIELD_PRODUCT_CJT_CODE = "CJT-" . $val->id;
			$FIELD_PRODUCT_MAIN_IMAGE = $main_image_hash;
			$FIELD_PRODUCT_DESCRIPTION = $result_description_match[1];
			$FIELD_PRODUCT_PROFIT = $FIELD_PRODUCT_CJT_PRICE - $FIELD_PRODUCT_MERCHANT_PRICE;

			$product_table = new Product;

			$product_table->PRODUCT_CATEGORY_ID = $FIELD_PRODUCT_ID_CATEGORY;
			$product_table->PRODUCT_CJT_CODE = $FIELD_PRODUCT_CJT_CODE;
			$product_table->PRODUCT_MERCHANT_ID = $FIELD_PRODUCT_MERCHANT_ID;
			$product_table->PRODUCT_NAME = $FIELD_PRODUCT_NAME;
			$product_table->PRODUCT_NAME_SLUG = $FIELD_PRODUCT_NAME_SLUG;
			$product_table->PRODUCT_MERCHANT_PRICE = $FIELD_PRODUCT_MERCHANT_PRICE;
			$product_table->PRODUCT_CJT_PRICE = $FIELD_PRODUCT_CJT_PRICE;
			$product_table->PRODUCT_PROFIT = $FIELD_PRODUCT_PROFIT;
			$product_table->PRODUCT_DESCRIPTION = $FIELD_PRODUCT_DESCRIPTION;
			$product_table->PRODUCT_MAIN_IMAGE = $FIELD_PRODUCT_MAIN_IMAGE;
			$product_table->PRODUCT_SHIPPING_ID = 12;
			$product_table->PRODUCT_STATUS_TEMP = 1;

			$product_table->save();

		}

		DB::table('product')->update(['PRODUCT_STATUS' => DB::raw("`PRODUCT_STATUS_TEMP`")]);
		DB::table('master_category')->update(['CATEGORY_STATUS' => DB::raw("`CATEGORY_STATUS_TEMP`")]);

	}
	public function hex_to_name($hex) {
		$hex = strToLower($hex);
		if ($hex[0] != '#') {
			$hex = "#" . $hex;
		}
		$colormap = array_change_key_case($this->colormap);
		$colormap = array_flip($colormap);
		if (array_key_exists($hex, $colormap)) {
			return $colormap[$hex];
		} else {
			return $hex;
		}
	}

	private function load_colormap() {
		$this->colormap = array(
			'AliceBlue' => '#f0f8ff',
			'AntiqueWhite' => '#faebd7',
			'Aqua' => '#00ffff',
			'Aquamarine' => '#7fffd4',
			'Azure' => '#f0ffff',
			'Beige' => '#f5f5dc',
			'Bisque' => '#ffe4c4',
			'Black' => '#000000',
			'BlanchedAlmond' => '#ffebcd',
			'Blue' => '#0000ff',
			'BlueViolet' => '#8a2be2',
			'Brown' => '#a52a2a',
			'Burlywood' => '#deb887',
			'CadetBlue' => '#5f9ea0',
			'Chartreuse' => '#7fff00',
			'Chocolate' => '#d2691e',
			'Coral' => '#ff7f50',
			'CornflowerBlue' => '#6495ed',
			'Cornsilk' => '#fff8dc',
			'Crimson' => '#dc143c',
			'Cyan' => '#00ffff',
			'DarkBlue' => '#00008b',
			'DarkCyan' => '#008b8b',
			'DarkGoldenrod' => '#b8860b',
			'DarkGray' => '#a9a9a9',
			'DarkGrey' => '#a9a9a9',
			'DarkGreen' => '#006400',
			'DarkKhaki' => '#bdb76b',
			'DarkMagenta' => '#8b008b',
			'DarkOliveGreen' => '#556b2f',
			'DarkOrange' => '#ff8c00',
			'DarkOrchid' => '#9932cc',
			'DarkRed' => '#8b0000',
			'DarkSalmon' => '#e9967a',
			'DarkSeaGreen' => '#8fbc8f',
			'DarkSlateBlue' => '#483d8b',
			'DarkSlateGray' => '#2f4f4f',
			'DarkSlateGrey' => '#2f4f4f',
			'DarkTurquoise' => '#00ced1',
			'DarkViolet' => '#9400d3',
			'DeepPink' => '#ff1493',
			'DeepSkyBlue' => '#00bfff',
			'DimGray' => '#696969',
			'DimGrey' => '#696969',
			'DodgerBlue' => '#1e90ff',
			'FireBrick' => '#b22222',
			'FloralWhite' => '#fffaf0',
			'ForestGreen' => '#228b22',
			'Fuchsia' => '#ff00ff',
			'Gainsboro' => '#dcdcdc',
			'GhostWhite' => '#f8f8ff',
			'Gold' => '#ffd700',
			'Goldenrod' => '#daa520',
			'Gray' => '#808080',
			'Grey' => '#808080',
			'Green' => '#008000',
			'GreenYellow' => '#adff2f',
			'Honeydew' => '#f0fff0',
			'HotPink' => '#ff69b4',
			'IndianRed' => '#cd5c5c',
			'Indigo' => '#4b0082',
			'Ivory' => '#fffff0',
			'Khaki' => '#f0e68c',
			'Lavender' => '#e6e6fa',
			'LavenderBlush' => '#fff0f5',
			'LawnGreen' => '#7cfc00',
			'LemonChiffon' => '#fffacd',
			'LightBlue' => '#add8e6',
			'LightCoral' => '#f08080',
			'LightCyan' => '#e0ffff',
			'LightGoldenrodYellow' => '#fafad2',
			'LightGray' => '#d3d3d3',
			'LightGrey' => '#d3d3d3',
			'LightGreen' => '#90ee90',
			'LightPink' => '#ffb6c1',
			'LightSalmon' => '#ffa07a',
			'LightSeaGreen' => '#20b2aa',
			'LightSkyBlue' => '#87cefa',
			'LightSlateGray' => '#778899',
			'LightSlateGrey' => '#778899',
			'LightSteelBlue' => '#b0c4de',
			'LightYellow' => '#ffffe0',
			'Lime' => '#00ff00',
			'Limegreen' => '#32cd32',
			'Linen' => '#faf0e6',
			'Magenta' => '#ff00ff',
			'Maroon' => '#800000',
			'MediumAquamarine' => '#66cdaa',
			'MediumBlue' => '#0000cd',
			'MediumOrchid' => '#ba55d3',
			'MediumPurple' => '#9370d8',
			'MediumSeaGreen' => '#3cb371',
			'MediumSlateBlue' => '#7b68ee',
			'MediumSpringGreen' => '#00fa9a',
			'MediumTurquoise' => '#48d1cc',
			'MediumVioletRed' => '#c71585',
			'MidnightBlue' => '#191970',
			'MintCream' => '#f5fffa',
			'MistyRose' => '#ffe4e1',
			'Moccasin' => '#ffe4b5',
			'NavajoWhite' => '#ffdead',
			'Navy' => '#000080',
			'OldLace' => '#fdf5e6',
			'Olive' => '#808000',
			'OliveDrab' => '#6b8e23',
			'Orange' => '#ffa500',
			'OrangeRed' => '#ff4500',
			'Orchid' => '#da70d6',
			'PaleGoldenrod' => '#eee8aa',
			'PaleGreen' => '#98fb98',
			'PaleTurquoise' => '#afeeee',
			'PaleVioletRed' => '#d87093',
			'PapayaWhip' => '#ffefd5',
			'PeachPuff' => '#ffdab9',
			'Peru' => '#cd853f',
			'Pink' => '#ffc0cb',
			'Plum' => '#dda0dd',
			'PowderBlue' => '#b0e0e6',
			'Purple' => '#800080',
			'Red' => '#ff0000',
			'RebeccaPurple' => '#663399',
			'RosyBrown' => '#bc8f8f',
			'RoyalBlue' => '#4169e1',
			'SaddleBrown' => '#8b4513',
			'Salmon' => '#fa8072',
			'SandyBrown' => '#f4a460',
			'SeaGreen' => '#2e8b57',
			'Seashell' => '#fff5ee',
			'Sienna' => '#a0522d',
			'Silver' => '#c0c0c0',
			'SkyBlue' => '#87ceeb',
			'SlateBlue' => '#6a5acd',
			'SlateGray' => '#708090',
			'SlateGrey' => '#708090',
			'Snow' => '#fffafa',
			'SpringGreen' => '#00ff7f',
			'SteelBlue' => '#4682b4',
			'Tan' => '#d2b48c',
			'Teal' => '#008080',
			'Thistle' => '#d8bfd8',
			'Tomato' => '#ff6347',
			'Turquoise' => '#40e0d0',
			'Violet' => '#ee82ee',
			'Wheat' => '#f5deb3',
			'White' => '#ffffff',
			'WhiteSmoke' => '#f5f5f5',
			'Yellow' => '#ffff00',
			'YellowGreen' => '#9acd32',
		);
	}
	public function cryptor($encrypt_decrypt, $string) {
		$password = 'ZsdjcxRURGUjZOOTVi';
		$method = 'aes-128-cbc';
		$iv = substr(hash('sha256', $password), 0, 16);
		$output = '';
		if ($encrypt_decrypt == 'encrypt') {
			$output = openssl_encrypt($string, $method, $password, 0, $iv);
			$output = base64_encode($output);
		} else if ($encrypt_decrypt == 'decrypt') {
			$output = base64_decode($string);
			$output = openssl_decrypt($output, $method, $password, 0, $iv);
		}
		return $output;
	}
	public function testCryptor() {
		$teks = '21';
		$enc = $this->cryptor('encrypt', $teks);
		$dec = $this->cryptor('decrypt', $enc);
		echo "$enc \n$dec";
	}

	public function get_tokopedia_merchant() {
		$merchant_url = 'https://ace.tokopedia.com/search/v2.6/product?shop_id=202414&ob=5&rows=8&start=0&full_domain=www.tokopedia.com&scheme=https&device=desktop&source=shop_product';

		$curl_data = $this->get_data($merchant_url);

		if (!$curl_data) // nothing grabbing
		{
			// display error message
			return Response::json(array(
				'code' => 503,
				'date' => date('Y-m-d h:i:sa'),
				'message' => 'Connection Error',

			));
			// send telegram report
		}

		$result = json_decode($curl_data);

		$number = 0;

		foreach ($result->data as $key => $val) {

			// --------------------< G E T  C A T E G O R Y >--------------------------
			$url_category = 'https://www.tokopedia.com/ajax/product-prev-next.pl?p_id=' . $val->id . '&action=prev_next&s_id=202414&lang=id';

			$get_data_category = $this->get_data($url_category);

			preg_match('/titl.*?"(.*?)"/', $get_data_category, $result_data_category);

			$get_merch = rtrim($result_data_category[1], '\\');

			if (substr($get_merch, 0, 10) === 'JAM TANGAN') {
				$get_merch = preg_replace("/\s\(.*?\)/", "", $get_merch);

				$get_merch = str_replace("JAM TANGAN ", "", $get_merch);
			} else {
				continue;
			}

			// --------------------< / G E T  C A T E G O R Y >-------------------------

			// --------------------<  D E T A I L  P R O D U C T >----------------------

			$url_detail = $this->get_data($val->uri);

			$format_regex = $this->clean_data($url_detail);

			preg_match('/\<div itemscope (.*) diskusi/', $format_regex, $result_description);

			preg_match('/itemprop="description">(.*?)rev/', $result_description[1], $result_description_match);

			$result_description_match[1];

			// --------------------< / D E T A I L  P R O D U C T >---------------------

			// --------------------< P R I C E  M A R K U P >---------------------------

			$val->price = preg_replace('/Rp/', '', $val->price);

			$val->price = preg_replace('/\./', '', $val->price);

			$toped_supplier_price = $val->price;

			if ($toped_supplier_price <= 50000) {

				$markup_percentige = 45;
			} else if (($toped_supplier_price > 50000) AND ($toped_supplier_price <= 125000)) {

				$markup_percentige = 35;
			} else if (($toped_supplier_price > 125000) AND ($toped_supplier_price <= 250000)) {

				$markup_percentige = 30;
			} else {

				$markup_percentige = 25;
			}

			// ------------------< / P R I C E  M A R K U P >--------------------------

			// --------------------< G E T  P R O D U C T  V A L U E >-----------------

			$FIELD_PRODUCT_NAME = $val->name;
			$FIELD_PRODUCT_MERCHANT_PRICE = $val->price;
			$FIELD_PRODUCT_CJT_PRICE = $val->price * ((100 + $markup_percentige) / 100);
			$FIELD_PRODUCT_MERCHANT_ID = $val->id;
			$FIELD_PRODUCT_CJT_CODE = "CJT-" . $val->id;
			//$FIELD_PRODUCT_MAIN_IMAGE = $main_image_hash;
			$FIELD_PRODUCT_DESCRIPTION = $result_description_match[1];

			$FIELD_CATEGORY_MERCH = $get_merch;
			$FIELD_CATEGORY_MERCH_SLUG = str_slug($FIELD_CATEGORY_MERCH, "-");
			$FIELD_CATEGORY_NAME = self::CATEGORY_NAME;
			$FIELD_CATEGORY_NAME_SLUG = str_slug($FIELD_CATEGORY_NAME, "-");

			DIE(" PASS");

			/*DB::table('category')->insert(
				  			  ['category_Name' => 'john@example.com', 'category_NameSlug' => 0,'category_Merch' => $category_merch, 'category_MerchSlug' => $category_merch_slug,'category_StatusTemp' => 1]
			*/

/*

$data = array(
'eclast_code'=>$eclast_code,
'id_category'=>$idkatgorinya,
'name'=>$val->name,
'supplier_code'=>$val->id,
'supplier_price'=>$val->price,
'eclast_price'=>$eclast_price,
'main_image'=>$main_image,
//'description'=>$deskripsi[1],
'slug'=>$slug,
'status_temp'=>1

 */

			// --------------------< / G E T  P R O D U C T  V A L U E >-----------------

		}

	}

	public function testDB() {
		$users = DB::table('category')->get();
		dd($users);
	}

	public function get_tokopedia2() {

		print_r($result);
	}

	public function readFile() {
		echo Storage::size('public/uploads/img_4f87a78ebdb8793da53601b3e04625e3_600x500.jpg');
		echo storage_path();

		echo "<br/>";

	}

	public function save_images() {
		$main_image = "cjt-" . $val->id;
		$main_image_hash = "img_" . md5($main_image . time());
		$main_image_hash_300X400 = "img_" . md5($main_image . time()) . "_300x400.jpg";
		$main_image_hash_600X500 = "img_" . md5($main_image . time()) . "_600x500.jpg";
		$folder_300X400 = 'storage/uploads/' . $main_image_hash_300X400;
		$folder_600X500 = 'storage/uploads/' . $main_image_hash_600X500;
		$toped_image = $val->image_uri_700; // maybe stored in parameter
		$image_exists = Storage::disk('local')->exists(self::IMG_PATH . 'img_4f87a78ebdb8793da53601b3e04625e3_600x500.jpg');

		if ($image_exists) {

			echo "gambar ada";
		} else {
			/*try
						{
						    $image_300x400 = \Image::make($toped_image);
						    $image_600x500 = \Image::make($toped_image);
						}
						catch(NotReadableException $e)
						{
						    // If error, stop and continue looping to next iteration
						    continue;
						}
						    // If no error ...
						$image_300x400->fit(300, 400, function ($constraint)
						{
						    $constraint->aspectRatio();
						});

				        $image_600x500->fit(600, 500, function ($constraint)
				        {
				        	$constraint->aspectRatio();
				        });
				        $image_300x400->save($folder_300X400);
			*/
		}

	}

	public function get_tokopedia() {
		$urlJam = 'https://ace.tokopedia.com/search/v2.6/product?shop_id=202414&ob=5&rows=3&start=0&full_domain=www.tokopedia.com&scheme=https&device=desktop&source=shop_product';
		//echo count($urlJam);
		//die($urlJam);
		$curl_data = $this->get_data($urlJam);

		$result = json_decode($curl_data);

		$ii = 1;

		$number = 0;

		foreach ($result->data as $key => $val) {

			$main_image = "cjt-" . $val->id;

			$main_image_hash_300X400 = "img_" . md5($main_image . time()) . "_300x400.jpg";
			$main_image_hash_600X500 = "img_" . md5($main_image . time()) . "_600x500.jpg";
			$folder_300X400 = 'storage/uploads/' . $main_image_hash_300X400;
			$folder_600X500 = 'storage/uploads/' . $main_image_hash_600X500;
			$toped_image = $val->image_uri_700;
			try
			{
				$image_300x400 = \Image::make($toped_image);
				$image_600x500 = \Image::make($toped_image);
			} catch (NotReadableException $e) {
				// If error, stop and continue looping to next iteration
				continue;
			}
			// If no error ...
			$image_300x400->fit(300, 400, function ($constraint) {
				$constraint->aspectRatio();
			});

			$image_600x500->fit(600, 500, function ($constraint) {
				$constraint->aspectRatio();
			});
			$image_300x400->save($folder_300X400);
			$image_600x500->save($folder_600X500);

		}
		die;

	}

	function lanjutan() {

		$file = Input::file('storage/uploads/test.jpg');

		Image::make($file->getRealPath())->resize('200', '200');
		die;

		// resize image instance
		$img->resize(320, 240);
		$img->insert('public/storage/uploads/watermark.png');

// save image in desired format
		$img->save('public/storage/uploads/bar.jpg');
		die;

		//$contents->resize(300, 200);
		file_put_contents($folder, file_get_contents($contents));

		//return $contents->response('jpg');
		die;

		Storage::disk('uploads')->put('file.txt', 'Contents');
		//Storage::disk('uploads')->put($main_image_hash, $contents);
		die;

		echo $number++;

		$val->id . "<br/>";

		$toped_slug_name = strtolower(preg_replace('/\s/', "-", $val->name));

		//echo $toped_name = $val->name;

		//echo $cjt_code="CJT-".$val->id;

		// --------------------< G E T  C A T E G O R Y >--------------------------

		$url_category = 'https://www.tokopedia.com/ajax/product-prev-next.pl?p_id=' . $val->id . '&action=prev_next&s_id=202414&lang=id';

		$get_data_category = $this->get_data($url_category);

		preg_match('/titl.*?"(.*?)"/', $get_data_category, $result_data_category);

		$category_merch = rtrim($result_data_category[1], '\\');

		$category_merch_slug = str_slug($category_name, "-");

		echo "<br/>" . $category_merch_slug . "<br/>";

		/*DB::table('category')->insert(
			  			  ['category_Name' => 'john@example.com', 'category_NameSlug' => 0,'category_Merch' => $category_merch, 'category_MerchSlug' => $category_merch_slug,'category_StatusTemp' => 1]
		*/

		// --------------------< / G E T  C A T E G O R Y >--------------------------

		// --------------------< P R I C E  M A R K U P >--------------------------

		$val->price = preg_replace('/Rp/', '', $val->price);

		$val->price = preg_replace('/\./', '', $val->price);

		$toped_supplier_price = $val->price;

		if ($toped_supplier_price <= 50000) {

			$markup_percentige = 45;
		} else if (($toped_supplier_price > 50000) AND ($toped_supplier_price <= 125000)) {

			$markup_percentige = 35;
		} else if (($toped_supplier_price > 125000) AND ($toped_supplier_price <= 250000)) {

			$markup_percentige = 30;
		} else {

			$markup_percentige = 25;
		}

		$cjt_price = $val->price * ((100 + $markup_percentige) / 100);

		echo $val->price . " jadi " . $cjt_price . "<br/>";

		// ------------------< / P R I C E  M A R K U P >--------------------------

		// --------------------< I M A G E >--------------------------

		$main_image = "cjt-" . $val->id;
		echo "<br/>";
		$main_image_hash = "img_" . md5($main_image . time()) . ".jpg";
		echo $main_image_hash;

		//echo base64_encode($main_image);
		echo "<br/>";

		// --------------------< / I M A G E >--------------------------

		//$datas=$this->get_data($val->uri);
		//$xxx=$this->clean_data($datas);
	}

	function get_data($url) {
		$ch = curl_init();
		$timeout = 500000000;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	function clean_data($data) {
		$web = $data;
		$web = str_replace("\n", '', $web);
		$web = str_replace("\r", '', $web);
		$web = str_replace("\r\n", '', $web);
		$web = str_replace("\n\r", '', $web);
		$web = str_replace(PHP_EOL, '', $web);
		return $web;
	}

}
