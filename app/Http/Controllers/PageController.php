<?php
namespace App\Http\Controllers;

use App\Http\Traits\ShippingTrait;
use App\Order;
use Cart;
use DB;
use Illuminate\Http\Request;
use URL;

class PageController extends Controller {
	use ShippingTrait;
	private $subTotalCart;
	private $countCart;

	public function cart() {
		$cartTotal = str_replace('.00', '', str_replace(',', '', Cart::subtotal()));
		return view('template_womensfashion.cart-page')->with('BASE_URL', URL::to('/'))->with('GET_PROVINCE', $this->getProvince())->with('CART_TOTAL', $cartTotal);
	}

	public function index() {
		$this->subTotalCart = Cart::subtotal();
		$this->countCart = Cart::count();

		//dd(Cart::content());

		$breadcrumb = '';
		$dataProduct = DB::table('product')
			->select('*')
			->orderBy('PRODUCT_CJT_PRICE', 'desc')
			->get();

		$countProduct = DB::table('product')->count();

		$dataSidebarCategory = DB::table('master_category')
		//->where('CATEGORY_STATUS', 1)
			->select('CATEGORY_MERCH', 'CATEGORY_MERCH_SLUG', 'CATEGORY_ORIGINAL')
			->orderBy('CATEGORY_MERCH', 'asc')
			->get();
		return view('template_womensfashion.homepage')->with('BASE_URL', URL::to('/'))->with('GET_PRODUCT', $dataProduct)->with('GET_MERCH', $dataSidebarCategory)->with('COUNT_PRODUCT', $countProduct)->with('BREADCRUMB', $breadcrumb)->with('SUB_TOTAL_CART', $this->subTotalCart)->with('COUNT_ITEM_CART', $this->countCart);
	}

	public function category($merch) {
		$this->subTotalCart = Cart::subtotal();
		$this->countCart = Cart::count();

		$breadcrumb = str_replace('-', ' ', strToUpper($merch));
		$page = 'CATEGORY';

		$dataProductPerCategory = DB::table('product')
			->join('master_category', 'master_category.CATEGORY_ID', 'product.PRODUCT_CATEGORY_ID')
			->where('master_category.CATEGORY_MERCH_SLUG', $merch)
			->select('*')
			->get();

		$countProductPerCategory = DB::table('product')
			->join('master_category', 'master_category.CATEGORY_ID', 'product.PRODUCT_CATEGORY_ID')
			->where('master_category.CATEGORY_MERCH_SLUG', $merch)
			->count();

		$dataSidebarCategory = DB::table('master_category')
		//->where('CATEGORY_STATUS', 1)
			->select('CATEGORY_MERCH', 'CATEGORY_MERCH_SLUG', 'CATEGORY_ORIGINAL')
			->orderBy('CATEGORY_MERCH', 'asc')
			->get();

		return view('template_womensfashion.category-page')->with('BASE_URL', URL::to('/'))->with('GET_PRODUCT', $dataProductPerCategory)->with('GET_MERCH', $dataSidebarCategory)->with('BREADCRUMB', $breadcrumb)->with('COUNT_PRODUCT', $countProductPerCategory)->with('SUB_TOTAL_CART', $this->subTotalCart)->with('COUNT_ITEM_CART', $this->countCart)->with('GET_NAME_MERCH', str_replace('-', ' ', strToUpper($merch)))->with('IS_PAGE', $page);
	}
	public function detail($slug) {

		$page = 'DETAIL';
		$this->subTotalCart = Cart::subtotal();
		$this->countCart = Cart::count();
		$this->subTotalCart = Cart::subtotal();

		preg_match('/(.*?)-/', $slug, $resultIdRegex);

		$idProduct = $resultIdRegex[1];

		$dataRelatedProduct = DB::table('product')
			->select('*')
			->inRandomOrder()
			->limit(8)
			->get();

		$dataProductDetail = DB::table('product')
			->join('master_category', 'master_category.CATEGORY_ID', 'product.PRODUCT_CATEGORY_ID')
			->where('product.PRODUCT_ID', $idProduct)
			->select('*', 'master_category.CATEGORY_MERCH', 'master_category.CATEGORY_MERCH_SLUG')
			->first();
		$breadcrumb = $dataProductDetail->PRODUCT_NAME;

		return view('template_womensfashion.detail-page')->with('DETAIL', $dataProductDetail)->with('BASE_URL', URL::to('/'))->with('BREADCRUMB', $breadcrumb)->with('RELATED_PRODUCT', $dataRelatedProduct)->with('SUB_TOTAL_CART', $this->subTotalCart)->with('COUNT_ITEM_CART', $this->countCart)->with('IS_PAGE', $page)->with('GET_NAME_MERCH', $dataProductDetail->CATEGORY_MERCH);
	}

	public function saveOrderForm(Request $request) {
		$uniqueCode = rand(10, 99);
		//echo $request->getProductCart;

		app('App\Http\Controllers\FunctionController')->TELEGRAM__invoiceNotif($request->getNama, $request->getSubTotal, $request->getProductCart);
		die;

		app('App\Http\Controllers\FunctionController')->MAIL__send($request->getEmail, $request->getProductCart, $request->getSubTotal, $request->getOngkir, $request->getCity, $uniqueCode);
		die;

		$uniqueCode = rand(10, 99);

		$this->orderNotif($request->getNama, $request->getSubTotal);
		app('App\Http\Controllers\FunctionController')->MAIL__send($request->getEmail);

		$order_table = new Order;

		$order_table->ORDER_PRODUCT_ALL = $request->getProductCart;
		$order_table->ORDER_GENERATE_ID = 'Not Set';
		$order_table->ORDER_ID_CUSTOMER = 0;
		$order_table->ORDER_NEW_NAME = $request->getNama;
		$order_table->ORDER_NEW_EMAIL = $request->getEmail;
		$order_table->ORDER_NEW_PHONE = $request->getTelphone;
		$order_table->ORDER_NEW_ADDRESS = $request->getAddress;
		$order_table->ORDER_NEW_CITY = $request->getCity;
		$order_table->ORDER_SUB_TOTAL = $request->getSubTotal;
		$order_table->ORDER_ONGKIR = $request->getOngkir;
		$order_table->ORDER_UNIQUE_CODE = $uniqueCode;

		$order_table->save();

		return $order_table;
//
	}

	public function orderNotif($name, $subTotal) {

		$text = 'Terima kasih ' . $name . ' atas pemesanan tas shopie martin black dengan No order : ECLORD-2141-189201 untuk pembayaran bisa di transfer di rekening BCA atas nama Denta Zamzam Yasin Muhajir dengan nominal Rp ' . $subTotal . '.setelah bayar kamu bisa konfirmasi lewat SMS/WA ke nomer ini dengan memberikan info no order dan nominal total transfer. Sekian Makasih';

		$url = 'https://api.telegram.org/bot462989956:AAEFIUgRVc5GRpzzxO1hG6F1nGyUR6iWaYs/sendMessage?chat_id=256823672&text=' . $text . '&parse_mode=html';
		return file_get_contents($url);

	}

}
